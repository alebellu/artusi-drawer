/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var drawerType = function(context, options) {
        var drawer = this;
        drawer.context = context;
        drawer.initOptions = options;
        drawer.declaredAttributes = drawer.initOptions.declaredAttributes;
        drawer.conf = drawer.initOptions.conf;
    };

    drawerType.loadDrawer = function(context, options) {
        var dr = $.Deferred();

        // load the drawer driver code
        var drawerConf = options.drawerConf;
        requirejs([drawerConf["sd:driver"]], function(specializedDrawerType) {
            var drawer = new drawerType(context, {
                declaredAttributes: drawerConf["sa:attributes"],
                conf: drawerConf["sd:conf"]
            });
            var specializedDrawer = new specializedDrawerType(context);
            //eval("specializedDrawer = new " + drawerConf['sd:name'] + "()");
            $.extend(drawer, specializedDrawer);
            drawer.init().done(function() {
                dr.resolve(drawer);
            }).fail(drff(dr));
        });

        return dr.promise();
    };

    drawerType.prototype.getIRI = function() {
        return this.conf["sd:iri"];
    };

    drawerType.prototype.getAttributes = function() {
        var attributes = {
            "sw:Drawer": true
        };
        if (this.getSpecificAttributes) {
            $.extend(attributes, this.getSpecificAttributes(), attributes);
        }
        if (this.declaredAttributes) {
            $.extend(attributes, this.declaredAttributes, attributes);
        }
        return attributes;
    };

    drawerType.prototype.applyContext = function(uri) {
        var drawer = this;

        if (drawer.context) {
            $.each(drawer.context, function(key, value) {
                uri = uri.replace(value, key + ":");
            });
        }

        return uri;
    };

   /**
     * Artusi implementation of SSOUP [drawer.knows](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.isAuthorized)
     *
     * @options:
     *     @resourceIRI the IRI of the resource
     *     @permissions permissions to check
     * @return true if specified permissions are granted for the given resource, false otherwise.
     */
    drawerType.prototype.isAuthorized = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var resourceIRI = drawer.applyContext(options.resourceIRI);
        var namespace = tools.getNamespace(resourceIRI);
dr.resolve(true);
        /*drawer.getResource({
            resourceIRI: tools.getIRI(namespace, "auth")
        }).done(function(auth) {
            var authorized = false;

            if (auth) {
                var username;
                if (drawer.conf["sd:authIRI"]) {
                    if (kitchen.shelf.authInfo[drawer.conf["sd:authIRI"]]) {
                        username = kitchen.shelf.authInfo[drawer.conf["sd:authIRI"]]["sa:userName"];
                        console.info("User: "+username);
                    }
                }

                var permissions = auth["sa:permissions"][resourceIRI];
                if (permissions) {
                    if (username && permissions[username] !== undefined) {
                        authorized = drawer.checkPermissions(permissions[username], options.permissions);
                    } else if (permissions["sa:public"] !== undefined) {
                        authorized = drawer.checkPermissions(permissions["sa:public"], options.permissions);
                    }
                }
            } else {
                // if no auth resource found => authorized by default
                authorized = true;
            }

            dr.resolve(authorized);
        }).fail(drff(dr));*/

        return dr.promise();
    };

    drawerType.prototype.checkPermissions = function(actual, requested) {
        for (var i = 0 ; i < requested.length ; i ++) {
            var permission = requested[i];
            if (actual.indexOf(permission) < 0) {
                return false;
            }
        }
        return true;
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.getIngredient)
     *
     * @options:
     *   @ingredientIRI the IRI of the resource to retrieve
     * @return the resource.
     */
    drawerType.prototype.getIngredient = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.isAuthorized({
            resourceIRI: options.ingredientIRI,
            permissions: "r"
        }).done(function(authorized) {
            if (authorized) {
                drawer.getResource({
                    resourceIRI: options.ingredientIRI,
                    mode: options.mode
                }).done(function(resource) {
                    var ingredient = resource;
                    if (ingredient && typeof ingredient === "string") {
                        ingredient = $.parseJSON(resource);
                    }
                    dr.resolve(ingredient);
                }).fail(drff(dr));
            } else {
                dr.reject("Unauthorized attempt to read " + options.ingredientIRI);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.put](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.putIngredient)
     *
     * @options:
     *   @ingredient the ingredient to save.
     */
    drawerType.prototype.putIngredient = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.isAuthorized({
            resourceIRI: options.ingredient["@id"],
            permissions: "w"
        }).done(function(authorized) {
            if (authorized) {
                drawer.put({
                    resourceIRI: options.ingredient["@id"],
                    data: options.ingredient,
                    message: "Update."
                }).drdf(dr).drff(dr);
            } else {
                dr.reject("Unauthorized attempt to write to " + options.ingredient["@id"]);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.putIngredient)
     *
     * @options:
     *   @ingredients the ingredients to save.
     */
    drawerType.prototype.putIngredients = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var putIngredient = function(index) {
            drawer.isAuthorized({
                resourceIRI: options.ingredients[index]["@id"],
                permissions: "w"
            }).done(function(authorized) {
                if (authorized) {
                    drawer.put({
                        resourceIRI: options.ingredients[index]["@id"],
                        data: options.ingredients[index],
                        message: "Update."
                    }).done(function() {
                        if (index < options.ingredients.length - 1) {
                            putIngredient(index + 1);
                        }
                        else {
                            dr.resolve();
                        }
                    }).fail(drff(dr));
                } else {
                    dr.reject("Unauthorized attempt to write to " + options.ingredients[index]["@id"]);
                }
            }).fail(drff(dr));
        };

        if (options.ingredients && options.ingredients.length > 0) {
            putIngredient(0);
        }
        else {
            dr.resolve();
        }

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.getTypes](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.getTypes)
     *
     * @options:
     *   @ingredient the ingredient for which to retrieve the types
     * @return the resource.
     */
    drawerType.prototype.getTypes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getIngredient({
            ingredientIRI: options.ingredient
        }).done(function(ingredient) {
            if (ingredient) {
                var types = ingredient["@type"];
                if (!$.isArray(types)) {
                    types = [types];
                }
                dr.resolve(types);
            }
            else {
                dr.resolve();
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.getSuperTypes](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.getSuperTypes)
     *
     * @options:
     *   @type the type for which to retrieve the super types
     * @return the super types
     */
    drawerType.prototype.getSuperTypes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        /**
        * Literals
        * From http://www-kasm.nii.ac.jp/~koide/SWCLOS2/Manual/06Datatypes.htm
        *
        * The string and number that appear in RDF are called literal.
        * Every literal in RDF is an instance of rdfs:Literal.
        * The class rdf:XMLLiteral is a subclass of rdfs:Literal and a class of all XML Schema data,
        * which includes xsd:string and xsd:decimal and so on. It implies that every XML Schema data
        * is also an instance of rdfs:Literal, and every literal is also an instance of rdfs:Resource,
        * because rdfs:Literal is a subclass of rdfs:Resource.
        */
        if (options.type === 'rdf:XMLLiteral') dr.resolve('rdfs:Literal');
        else if (options.type === 'rdfs:Literal') dr.resolve('rdfs:Resource');
        /**
        * Non-Literals
        */
        else {
            drawer.getIngredient({
                ingredientIRI: options.type
            }).done(function(type) {
                if (type && type["rdfs:subClassOf"]) {
                    var superTypes = type["rdfs:subClassOf"];
                    if (!$.isArray(superTypes)) {
                        superTypes = [superTypes];
                    }
                    dr.resolve(superTypes);
                }
                else {
                    dr.resolve(["rdfs:resource"]);
                }
            }).fail(drff(dr));
        }

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.getRecipe](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.getRecipe)
     *
     * @options
     *    @recipeIRI the IRI of the recipe to retrieve
     * @return the info about the recipe.
     */
    drawerType.prototype.getRecipe = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.fetchRecipes().done(function(recipes) {
            if (drawer.recipes) {
                var recipeImpls = drawer.recipes["sr:recipeImpls"];
                var recipeInterfaces = drawer.recipes["sr:recipeInterfaces"];
                var recipeImpl = recipeImpls[options.recipeIRI];
                if (recipeImpl) {
                    var recipeInterfaceName = recipeImpl["sr:interface"];
                    var recipeInterface = recipeInterfaces[recipeInterfaceName];
                    dr.resolve({
                        interface: recipeInterface,
                        impl: recipeImpl
                    });
                } else {
                    dr.resolve();
                }
            } else {
                dr.resolve();
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.getTypeProperties](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.getTypeProperties)
     *
     * @options
     *    @type the type
     * @return the properties of the type.
     */
    drawerType.prototype.getTypeProperties = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.fetchTypes().done(function(types) {
            if (drawer.typesProperties) {
                dr.resolve(drawer.typesProperties[options.type]);
            } else {
                dr.resolve();
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.listRecipes](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.listRecipes)
     *
     * @options
     *    @inputType the type of the ingredient for which to look for recipes
     *	@categories the list of categories the returned recipes has to belong to
     * @return a list of interfaces of the recipes and reference to their implementation that can be called for the given resource
     */
    drawerType.prototype.listRecipes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var filterRecipes = function() {
            var filteredRecipes = [];
            var recipeImpls = drawer.recipes["sr:recipeImpls"];
            var recipeInterfaces = drawer.recipes["sr:recipeInterfaces"];
            for (var recipeImplURI in recipeImpls) {
                var recipeImpl = recipeImpls[recipeImplURI];
                var recipeInterfaceName = recipeImpl["sr:interface"];
                var recipeInterface = recipeInterfaces[recipeInterfaceName];
                var recipeCategories = recipeInterface["sr:hasCategory"];
                if (recipeCategories && !$.isArray(recipeCategories)) {
                    recipeCategories = [recipeCategories];
                }

                if (options.categories) {
                    var categoriesMatchFound = true;
                    $.each(options.categories, function(index, category) {
                        if ($.inArray(category, recipeCategories) == -1) {
                            categoriesMatchFound = false;
                        }
                    });
                    if (!categoriesMatchFound) {
                        continue;
                    }
                }

                var inputParametersDecl = recipeInterface["sr:hasInput"];
                if (!$.isArray(inputParametersDecl)) {
                    inputParametersDecl = [inputParametersDecl];
                }
                for (var i in inputParametersDecl) {
                    if (inputParametersDecl[i]["sr:hasType"] == options.inputType) {
                        if (!recipeImpl["sw:path"]) {
                            recipeImpl["sw:path"] = [{
                                "@type": "sr:Registry",
                                "sr:drawerIRI": drawer.getIRI()
                            }];
                        }
                        filteredRecipes.push({
                            interface: recipeInterface,
                            impl: recipeImpl
                        });
                    }
                }
            }
            dr.resolve(filteredRecipes);
        };

        drawer.fetchRecipes().done(function(recipes) {
            if (drawer.recipes) {
                filterRecipes();
            } else {
                dr.resolve();
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    drawerType.prototype.fetchTypes = function() {
        var dr = $.Deferred();
        var drawer = this;

        if (drawer.types) {
            dr.resolve(drawer.types);
        }
        else {
            drawer.fetchResourceAsJSONLD({
                resourceIRI: 'types'
            }).done(function(all){
                drawer.types = {};
                drawer.typesProperties = {};

                if (all) {
                    $.each(all, function(i, subject) {
                        if (subject["@type"] == 'rdfs:Class') {
                            drawer.types[subject["@id"]] = subject;
                        }

                        if (subject["@type"] == 'rdf:Property') {
                            if (!drawer.typesProperties[subject["rdfs:domain"]["@id"]]) {
                                drawer.typesProperties[subject["rdfs:domain"]["@id"]] = {};
                            }
                            drawer.typesProperties[subject["rdfs:domain"]["@id"]][subject["@id"]] = subject;
                        }
                    });
                }
                dr.resolve(drawer.types);
            }).fail(drff(dr));
        }

        return dr.promise();
    };

    drawerType.prototype.fetchRecipes = function() {
        var dr = $.Deferred();
        var drawer = this;

        if (drawer.recipes) {
            dr.resolve(drawer.recipes);
        }
        else {
            drawer.knows({
                resourceIRI: ".recipes.jsonld"
            }).done(function(known) {
                if (!known) {
                    dr.resolve(); // this drawer does not contain recipes
                    return dr.promise();
                }

                drawer.getResource({
                    resourceIRI: ".recipes.jsonld"
                }).done(function(si) {
                    if (si) {
                        drawer.recipes = $.parseJSON(si);
                    }
                    else {
                        drawer.recipes = {};
                    }
                    dr.resolve(drawer.recipes);
                }).fail(drff(dr));
            }).fail(drff(dr));
        }

        return dr.promise();
    };

    /**
     * Fetches a resource with the given name
     **/
    drawerType.prototype.fetchResourceAsJSONLD = function(options) {
        var dr = $.Deferred();
        var drawer = this;
        var kitchen = drawer.context.kitchen;

        var type;
        if (options.resourceIRI.match(/.xml$/)) {
            type = "rdf/xml";
        } else if (options.resourceIRI.match(/.jsonld$/)) {
            type = "rdf/json-ld";
        } else if (options.resourceIRI.match(/.ttl$/)) {
            type = "rdf/turtle";
        } else if (options.resourceIRI.match(/.n3$/)) {
            type = "rdf/n3";
        } else {
            // kitchen.tools.detectRDFFormat()
        }

        var fetchAndParseResource = function(resourceIRI, type) {
            drawer.getResource({
                resourceIRI: resourceIRI
            }).done(function(resource) {
                if (type == "rdf/json-ld") {
                    dr.resolve(resource);
                } else if (type == "rdf/turtle") {
                    kitchen.tools.renderTurtleContext(drawer.context).done(function(turtleContext) {
                        resource = turtleContext + '\n' + resource;
                        kitchen.tools.convertTurtleToJSONLD(resource, drawer.context).done(drdf(dr)).fail(drff(dr));
                    }).fail(drff(dr));
                } else if (type == "rdf/n3") {
                     dr.reject("rdf/n3 not supported at the moment.");
                } else if (type == "rdf/xml") {
                    dr.reject("rdf/xml not supported at the moment.");
                }
            }).fail(drff(dr));
        };

        drawer.knows({
            resourceIRI: options.resourceIRI
        }).done(function(known) {
            if (known) {
                if (!type) {
                    dr.reject("Type is unknown for resource.");
                }
                fetchAndParseResource(options.resourceIRI, type);
            } else {
                drawer.knows({
                    resourceIRI: options.resourceIRI + ".ttl"
                }).done(function(known) {
                    if (known) {
                        fetchAndParseResource(options.resourceIRI + ".ttl", "rdf/turtle");
                    } else {
                        drawer.knows({
                            resourceIRI: options.resourceIRI + ".n3"
                        }).done(function(known) {
                            if (known) {
                                fetchAndParseResource(options.resourceIRI + ".n3", "rdf/n3");
                            } else {
                                drawer.knows({
                                    resourceIRI: options.resourceIRI + ".jsonld"
                                }).done(function(known) {
                                    if (known) {
                                        fetchAndParseResource(options.resourceIRI + ".jsonld", "rdf/json-ld");
                                    } else {
                                        // "Resource not found"
                                        dr.resolve(undefined);
                                    }
                                }).fail(drff(dr));
                            }
                        }).fail(drff(dr));
                    }
                }).fail(drff(dr));
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.listRecipesCategories](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.listRecipes)
     *
     * @options
     * @return a list of recipes categories
     */
    drawerType.prototype.listRecipesCategories = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.fetchRecipes().done(function(recipes) {
            if (drawer.recipes) {
                var recipesCategories = {};
                var recipeInterfaces = drawer.recipes["sr:recipeInterfaces"];
                $.each(recipeInterfaces, function(recipeInterfaceIRI, recipeInterface) {
                    var recipeCategories = recipeInterface["sr:hasCategory"];
                    if (recipeCategories && !$.isArray(recipeCategories)) {
                        recipeCategories = [recipeCategories];
                    }
                    $.each(recipeCategories, function(index, recipeCategory) {
                        recipesCategories[recipeCategory] = true;
                    });
                });

                var keys = [];
                for(var k in recipesCategories) keys.push(k);
                dr.resolve(keys);
            } else {
                dr.resolve([]);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [driver.loadServiceCode](https://github.com/alebellu/ssoup/blob/master/concepts.md#driver.loadServiceCode)
     *
     * @options:
     * 	@serviceIRI the IRI of the service
     *  @code the code of the service
     */
    drawerType.prototype.storeServiceCode = function(options) {
        var resourceIRI = options.serviceIRI;
        resourceIRI = resourceIRI.replace('http://localhost/', '');
        return $(this).put({
            resourceIRI: resourceIRI,
            data: options.code
        });
    };

    /**
     * findPreferredRecipe will find the most specific preferred recipe with the given categories, compatible with given ingredient and type, for the given user. If no user is specified or if the user has not expressed specific preferences for the given categories (or subgroup of categories), global recipes preferences should be considered.
     *
    * @options:
    *   @user the user for whom to look up the preference
    *   @recipeCategories the set of categories to which the preference should be associated
    *   @ingredientIRI the IRI of the ingredient for which to find the preferred recipe.
    *   @typeHierarchy the type hierachy of the ingredient for which to find the preferred recipe.
     */
    drawerType.prototype.findPreferredRecipe = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var categories = options.recipesCategories;
        if (!$.isArray(categories)) {
            categories = [categories];
        }
        categories.sort();

        drawer.fetchRecipesPrefs({user: options.user}).done(function(recipesPrefs) {
            if (!recipesPrefs) {
                dr.resolve();
                return;
            }
            var pr = recipesPrefs;
            var prl = [];
            prl.push(pr);
            $.each(categories, function(index, category) {
                if (!recipesPrefs[category]) {
                    return false; // break
                }
                pr = recipesPrefs[category];
                prl.push(pr);
            });
            // now iterate in reverse order to find the most specific preference
            var preferredRecipeIRI;
            $(prl.reverse()).each(function(index, p) {
                if (options.ingredientIRI && p.ingredients && p.ingredients[options.ingredientIRI]) {
                    preferredRecipeIRI = p.ingredients[options.ingredientIRI];
                    return false; // break
                }
                var type = options.typeHierarchy["@type"];
                var superTypes = options.typeHierarchy["@superTypes"];
                if (type && p.types && p.types[type]) {
                    preferredRecipeIRI = p.types[options.ingredientType];
                    return false; // break
                }
                if (superTypes) {
                    $.each(superTypes, function(index, superType) {
                        if (p.types && p.types[superType]) {
                            preferredRecipeIRI = p[superType];
                            return false; // break
                        }
                    });
                }
            });
            dr.resolve(preferredRecipeIRI);
        }).fail(drff(dr));

        return dr.promise();
    }

    /**
    * savePreferredRecipe will save the most specific preferred recipe with the given categories, compatible with given ingredient and type, for the given user. If no user is specified the global recipes preferences will be used to store the info.
    * Please note that no checks are performed as to whether the specified recipe has the indicated categories and has a parameter with the type specified: it is the caller responsibility to perform such a check before calling the method.
    *
    * Sample .recipes.prefs.jsonld:
    *  {
    *   "sr:artusi": {
    *        "sr:viewer": {
    *            types: {
    *                "sdoc:paragraph": "sdoc: paragraphViewer"
    *            },
    *            ingredients: {
    *                "http://..../paragraph01": "sdoc:resourceViewer"
    *            }
    *        },
    *        types: {
    *            "rdfs:resource": "ssoup:resourceViewer"
    *        }
    *    }
    *  }
    *
    * @options:
    *   @user the user for whom to save the preference
    *   @recipeCategories the set of categories for which to save the preference
    *   @ingredientIRI the IRI of the ingredient to which associate the preference
    *   @type the type to which associate the preference
    *   @recipeIRI the IRI of the recipe.
    */
    drawerType.prototype.savePreferredRecipe = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var categories = options.recipesCategories;
        if (!$.isArray(categories)) {
            categories = [categories];
        }
        categories.sort();

        drawer.fetchRecipesPrefs({user: options.user}).done(function(recipesPrefs) {
            if (!recipesPrefs) {
                recipesPrefs = {};
            }
            var pr = recipesPrefs;
            $.each(categories, function(index, category) {
                if (!recipesPrefs[category]) {
                    recipesPrefs[category] = {};
                }
                pr = recipesPrefs[category];
            });
            if (options.ingredientIRI) {
                if (!pr.ingredients) {
                    pr.ingredients = {};
                }
                pr.ingredients[options.ingredientIRI] = options.recipeIRI;
            } else if (options.type) {
                if (!pr.types) {
                    pr.types = {};
                }
                pr.types[options.type] = options.recipeIRI;
            }

            var prefsFileName;
            if (!options.user) {
                prefsFileName = ".recipes.prefs.jsonld";
            } else {
                prefsFileName = ".recipes.prefs." + options.user + ".jsonld";
            }

            recipesPrefs["@id"] = prefsFileName;
            drawer.put({
                resourceIRI: prefsFileName,
                data: recipesPrefs,
                message: "Update recipes preferences."
            }).done(function() {
                // update cached recipes prefs
                drawer.recipesPrefs = recipesPrefs;
                dr.resolve();
            }).fail(drff(dr));
        }).fail(drff(dr));

        return dr.promise();
    }

    drawerType.prototype.fetchRecipesPrefs = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var user = options.user;
        var prefsFileName;
        if (!user) {
            user = "public";
            prefsFileName = ".recipes.prefs.jsonld";
        } else {
            prefsFileName = ".recipes.prefs." + user + ".jsonld";
        }

        if (!drawer.recipesPrefs) {
            drawer.recipesPrefs = {};
        }

        if (drawer.recipesPrefs[user]) {
            dr.resolve(drawer.recipesPrefs[user]);
        }
        else {
            drawer.knows({
                resourceIRI: prefsFileName
            }).done(function(known) {
                if (!known) {
                    dr.resolve(); // this drawer does not contain recipes prefs
                    return dr.promise();
                }

                drawer.getResource({
                    resourceIRI: prefsFileName
                }).done(function(prefs) {
                    if (prefs && typeof prefs === "string") {
                        prefs = $.parseJSON(prefs);
                    }

                    if (prefs) {
                        drawer.recipesPrefs[user] = prefs;
                    }
                    else {
                        drawer.recipesPrefs[user] = {};
                    }
                    dr.resolve(drawer.recipesPrefs[user]);
                }).fail(drff(dr));
            }).fail(drff(dr));
        }

        return dr.promise();
    };

    /**
   * Artusi implementation of SSOUP [driver.isCapableOf](https://github.com/alebellu/ssoup/blob/master/concepts.md#driver.isCapableOf)
   *
   * @options:
   * 	@capability the capability to check the presence of.
     @return true if the capability is supported by the driver, false otherwise.
   */
    drawerType.prototype.isCapableOf = function(options) {
        return (drawerType.prototype[options.capability] !== undefined);
    };

    drawerType.prototype.getRelativeResourceIRI = function(resourceIRI) {
        if (resourceIRI.indexOf('http') === 0 || resourceIRI.indexOf('https') === 0) {
            if (resourceIRI.indexOf(this.conf["sd:iri"]) === 0) {
                resourceIRI = resourceIRI.replace(this.conf["sd:iri"], '');
            }
            else {
                return;
            }
        }
        return resourceIRI;
    };

    drawerType.prototype.completeIngredient = function(ingredientIRI, ingredient) {
        var drawer = this;

        if (ingredient) {
            if (!ingredient["@id"]) {
                ingredient["@id"] = ingredientIRI;
            }
            if (!ingredient["sw:provenance"]) {
                ingredient["sw:provenance"] = {};
            }
            if (!ingredient["sw:provenance"]["prov:atLocation"]) {
                ingredient["sw:provenance"]["prov:atLocation"] = drawer.getIRI();
            }
        }
    };

    /**
     * Checks whether the drawer knows about the pan.
     *
     * @options
     *   @panIRI the IRI of the pan
     */
    drawerType.prototype.knowsPan = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getPans().done(function(pans) {
            if (pans[options.panIRI]) {
                dr.resolve(true);
            }
            else {
                dr.resolve(false);
            }
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Retrieve the pan with the given IRI.
     *
     * @options
     *   @panIRI the IRI of the pan to retrieve.
     */
    drawerType.prototype.getPan = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getPans().done(function(pans) {
            drawer.getResource({
                resourceIRI: pans[options.panIRI]["sc:panImpl"]
            }).done(drdf(dr)).fail(drff(dr));
        }).fail(drdf(dr));

        return dr.promise();
    };

    /**
     * Retrieves the list of pans in the drawer.
     *
     * @options
     */
    drawerType.prototype.getPans = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        if (drawer.pans) {
            dr.resolve(drawer.pans);
        }
        else {
            drawer.getResource({
                resourceIRI: "pans"
            }).done(function(pans) {
                drawer.pans = pans;
                dr.resolve(drawer.pans);
            }).fail(drdf(dr));
        }

        return dr.promise();
    };

    /**
     * @param options
     *   @message the message to be processed.
     */
    drawerType.prototype.onMessage = function(options) {
        var dr = $.Deferred();
        var drawer = this;
        var msg = options.message;

        if (msg["@type"] == "sm:task") {
            if (msg["sm:taskType"] == "sm:getRecipe") {
                drawer.getRecipe({
                    recipeIRI: msg["sm:taskParameters"]["sp:recipeIRI"]
                }).done(function(recipe) {
                    dr.resolve(recipe);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:listRecipes") {
                drawer.listRecipes({
                    inputType: msg["sm:taskParameters"]["sp:inputType"],
                    categories: msg["sm:taskParameters"]["sp:categories"]
                }).done(function(recipes) {
                    dr.resolve(recipes);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:listRecipesCategories") {
                drawer.listRecipesCategories().done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getResource") {
                drawer.getResource(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getIngredient") {
                drawer.getIngredient(msg["sm:taskParameters"]).done(function(ingredient) {
                    drawer.completeIngredient(msg["sm:taskParameters"]["ingredientIRI"], ingredient);
                    dr.resolve(ingredient);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:putIngredient") {
                drawer.putIngredient(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:putIngredients") {
                drawer.putIngredients(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getTypes") {
                drawer.getTypes({
                    ingredient: msg["sm:taskParameters"]["sp:ingredient"],
                }).done(function(types) {
                    dr.resolve(types);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getSuperTypes") {
                drawer.getSuperTypes({
                    type: msg["sm:taskParameters"]["sp:type"],
                }).done(function(superTypes) {
                    dr.resolve(superTypes);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getTypeProperties") {
                drawer.getTypeProperties({
                    type: msg["sm:taskParameters"]["sp:type"]
                }).done(function(typeProperties) {
                    dr.resolve(typeProperties);
                }).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:getServiceRecipes") {
                if (drawer.getServiceRecipes) {
                    drawer.getServiceRecipes(msg["sm:taskParameters"]).done(function(serviceRecipes) {
                        dr.resolve(serviceRecipes);
                    }).fail(drff(dr));
                }
                else {
                    dr.resolve();
                }
            }
            else if (msg["sm:taskType"] == "sm:executeRecipe") {
                var recipeName = msg["sm:taskParameters"].impl["sr:recipeName"];
                if (drawer[recipeName]) {
                    drawer[recipeName].apply(this, msg["sm:taskParameters"]);
                }
                else {
                    dr.resolve();
                }
            }
            else if (msg["sm:taskType"] == "sm:findPreferredRecipe") {
                drawer.findPreferredRecipe(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:savePreferredRecipe") {
                drawer.savePreferredRecipe(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
            }
            else if (msg["sm:taskType"] == "sm:buildIndexes") {
                if (drawer.buildIndexes) {
                    drawer.buildIndexes(msg["sm:taskParameters"]).done(drdf(dr)).fail(drff(dr));
                }
                else {
                    dr.resolve();
                }
            }
            else {
                dr.resolve();
            }
        }
        else {
            dr.resolve();
        }

        return dr.promise();
    };

    return drawerType;
});
